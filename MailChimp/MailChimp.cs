using System;
using DataAccess;
using System.Collections.Generic;
using System.IO;

namespace Memborg{

    public class MailChimp{

        static string inputfile = string.Empty;
        static string outputFile = "mailchimp.csv";
        public MailChimp(){
        }

        static void Main(string[] args){

            if(args.Length == 0)
            {
                Console.WriteLine("Du skal som minimum angive stien til filen du vil læse fra");
                return;
            }

            inputfile = args[0];
            if(!File.Exists(inputfile))
            {
                throw new ArgumentException("Filen du vil læse fra findes ikke!");
            }

            if(args.Length == 2)
            {
                outputFile = args[1];

                if (!string.IsNullOrWhiteSpace(outputFile) && !File.Exists(outputFile))
                {
                    throw new ArgumentException("Filen du vil gemme til findes ikke!");
                }
            }

            MutableDataTable dt = DataTable.New.ReadCsv(inputfile);

            List<string> RemoveColumns = new List<string>(){
                "Telefon",
                    "Mobiltelefon",
                    "By",
                    "Postnummer",
                    "Adresse",
                    "Lokalafdeling",
                    "Medlemstype",
                    "Oprettelsesdato",
                    "Køn"
            };

            dt.DeleteColumns(RemoveColumns.ToArray());
            dt.ReorderColumn("E-mail",0);
            dt.ReorderColumn("Medlemsnr.",3);

            dt.RenameColumn("Medlemsnr.", "Medlemsnummer");
            dt.RenameColumn("Har MS", "Har du sclerose");

            foreach (Row row in dt.Rows) {
                if(!string.IsNullOrEmpty(row["Fødselsdag"])){
                    DateTime myDate = DateTime.Parse(row["Fødselsdag"], new System.Globalization.CultureInfo("da-DK"));
                    row["Fødselsdag"] = myDate.Year.ToString();
                }
            }

            dt.KeepRows(HasEmail);

            using(StreamWriter sw = new StreamWriter(outputFile.Trim())){
                dt.SaveToStream(sw);
            }
        }

        private static bool HasEmail(Row r){
            return !string.IsNullOrEmpty(r["E-mail"]);
        }
    }
}
